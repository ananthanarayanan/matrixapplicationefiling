import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;


public class middleinit {
	private WebDriver driver;
	

    @Before
	public void get() throws IOException
	{
		
		Properties s=new Properties();
		FileInputStream k=new FileInputStream("C:\\Users\\Ananthanarayanan\\git\\matrixapplicationefiling\\matrixservices\\prop.properties");
		s.load(k);
		driver=new FirefoxDriver();
		driver.get(s.getProperty("url"));
		driver.manage().window().maximize();
		
	}
	
	@SuppressWarnings({ "rawtypes", "unused" })
	@Test
	public void test() throws IOException, InterruptedException
	{
		Properties s=new Properties();
		FileInputStream f=new FileInputStream("C:\\Users\\Ananthanarayanan\\git\\matrixapplicationefiling\\matrixservices\\prop.properties");
		s.load(f);
		
		
		//User login		
				driver.get(s.getProperty("url"));
				driver.findElement(By.xpath(s.getProperty("uname"))).sendKeys("test.user2@matrixcos.com");
				driver.findElement(By.xpath(s.getProperty("pwd"))).sendKeys("Test1234");
				driver.findElement(By.cssSelector(s.getProperty("clk1"))).click();
				driver.findElement(By.cssSelector(s.getProperty("clk2"))).click();
				driver.findElement(By.xpath(s.getProperty("clk3"))).click();


				
				//Transfer control to new window
				Set windowHandles = driver.getWindowHandles();
				Iterator it = windowHandles.iterator();
				String parentBrowser= (String) it.next();
				String childBrowser = (String) it.next();
				driver.switchTo().window(childBrowser);
				
				
				
				
				//search hr data
				driver.findElement(By.id(s.getProperty("id"))).sendKeys("1329");
				Thread.sleep(1000);
				driver.findElement(By.xpath(s.getProperty("lname"))).sendKeys("as");
				Thread.sleep(1000);
				driver.findElement(By.xpath(s.getProperty("dateenter"))).sendKeys("03/10/2014");
				Thread.sleep(1000);
				driver.findElement(By.xpath(s.getProperty("search"))).click();
				
		        
				
				//selecting the claimant
				Thread.sleep(2000);
				WebElement element4=driver.findElement(By.xpath(s.getProperty("select1")));
				Actions act=new Actions(driver);
				act.moveToElement(element4);
				act.build().perform();
				element4.click();

				
				
				//selecting the leave type		
				Thread.sleep(2000);
				driver.findElement(By.xpath(s.getProperty("lclick1"))).click();
				Thread.sleep(2000);
				WebElement element6=driver.findElement(By.xpath(s.getProperty("dpdwn")));
				Select cust = new Select(element6);
				Thread.sleep(2000);
				cust.selectByIndex(2);
				element6.click();		
				Thread.sleep(2000);		
				driver.findElement(By.xpath(s.getProperty("lclick2"))).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath(s.getProperty("lclick3"))).click();
				
				
				
				//filing intakes
				driver.findElement(By.xpath(s.getProperty("minit"))).clear();
				Thread.sleep(2000);	
				
				
					driver.findElement(By.xpath(s.getProperty("minit"))).sendKeys("&a6");
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(s.getProperty("homephno"))).sendKeys("56600234584");
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(s.getProperty("myeacc"))).click();
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(s.getProperty("reasforleav"))).sendKeys("fever");
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(s.getProperty("dateclr"))).sendKeys("02/04/2014");
					Thread.sleep(2000);	
					
					
					driver.findElement(By.xpath(s.getProperty("todet"))).click();
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(s.getProperty("reqleave"))).click();
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(s.getProperty("person"))).click();
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(s.getProperty("next"))).click();
					Thread.sleep(2000);
					
					//driver.findElement(By.xpath(s.getProperty("previ"))).click();
					
		}
				
@After
public void end() throws InterruptedException
{
	Thread.sleep(5000);
	driver.quit();
}	
	

}
